/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.vamk.e1801699;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author sampp
 */
public class WeekdayChecker {
    
    public String helloWeekday(String day) {
        if(day == null) {
            return "Hello World!";
        }
        
        Calendar c = Calendar.getInstance();
        SimpleDateFormat format1=new SimpleDateFormat("dd/MM/yyyy");
        try {
            Date dt1=format1.parse(day);
            c.setTime(dt1);
            int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
            String wd = weekday(dayOfWeek);
            
            return "Hello " + wd + " World!";
            
            
            
        } catch (ParseException ex) {
            return "Hello World!";
        }
            
    }
    
    public String weekday(int x) {
        switch (x) {
            case 1:
                return "Sunday";
            case 2:
                return "Monday";
            case 3:
                return "Tuesday";
            case 4:
                return "Wednesday";
            case 5:
                return "Thursday";
            case 6:
                return "Friday";
            case 7:
                return "Saturday";
                    
            default:
                
                break;
        }
        return null;
    }
}
