package fi.vamk.e1801699;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        WeekdayChecker wc = new WeekdayChecker();
        System.out.println(wc.helloWeekday("16/03/2020"));
        System.out.println(wc.helloWeekday("06/02/1997"));
        System.out.println(wc.helloWeekday(null));
    }
    
    
}
