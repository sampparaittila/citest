package fi.vamk.e1801699;


import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import org.junit.Test;


/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    WeekdayChecker wc = new WeekdayChecker();
    
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }
    
    @Test
    public void shouldReturnMonday(){
        assertEquals("Hello Monday World!", wc.helloWeekday("16/03/2020"));
    }
    
    @Test
    public void shouldReturnTuesday(){
        assertEquals("Hello Tuesday World!", wc.helloWeekday("17/03/2020"));
    }
    
    @Test
    public void shouldReturnHelloWithEmpty(){
        assertEquals("Hello World!", wc.helloWeekday(""));
    }
    
    @Test
    public void shouldReturnHelloWithNull(){
        assertEquals("Hello World!", wc.helloWeekday(null));
    }
    
    
}
